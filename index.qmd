---
title: "Bienvenidos a Voces de El Palo"
listing:
  contents: posts
  sort: "date desc"
  type: grid
  categories: true
  sort-ui: false
  filter-ui: false
page-layout: full
title-block-banner: true
---

**Voces de El Palo** es una revista digital sobre filosofía que pretende reflejar la vida intelectual del barrio de El Palo de Málaga. Aquí encontrará artículos escritos por y para personas humanistas, que valoran por encima de todo el conocimiento, la cultura, la naturaleza, la ciencia, y la experiencia vital.

<!-- Google tag (gtag.js) -->

```{=html}
<script async src="https://www.googletagmanager.com/gtag/js?id=G-MFRJGX7Z3Q"></script>
```

```{=html}
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-MFRJGX7Z3Q');
</script>
```
