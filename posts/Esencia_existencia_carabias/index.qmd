---
title: "Esencia y existencia; Ser y estar; Real y relativo"
author: "Manuel Carabias"
date: "2024-06-07"
categories: [Filosofía, Ser, Móvil]
image: "image.jpg"
draft: false
---


"Del gusano al homo hay un abismo, pero aún queda en nosotros mucho de gusano" (Friedrich Nietzsche, 1844-1900).

# Esencia y existencia.

## Ser y estar.

### Real y relativo.

Es obligado distinguir entre el *sistere* en que reside la naturaleza y el *exsistere* en que consiste ser persona. Desde hace aproximadamente tres millones de años, en el planeta Tierra, empiezan a surgir seres bípedos conscientes de sí, capaces de individualidad (el que, a quien y aquel de quien se habla): aparecen sujetos capaces de relacionarse consigo mismos y con el mundo (yo, tú, él). **Surge la paradoja de la realidad que se opone a sí misma** (El Uni-verso "busca" al observador. Carl Sagan, 1934-1996). Ese ser-ahí no solo se mira como existente, sino que debe hacerse así mismo. A partir de ese momento, el yo pasa a ocupar el centro de la aprehensión de las relaciones, con el consiguiente peligro personal de que a cada instante puedan surgir actos novedosos, correctos o erróneos. Surge entonces el ente frágil, capaz de descubrir dioses y fabricar utensilios en beneficio propio. Por ello, el existir humano es libre de manifestar la vida como un acontecer feliz, volcándose en el "se dice o se cuenta" de otros: "¿Qué voy a hacerle yo? si me gusta el güisqui sin soda, el sexo sin boda, las penas con pan..." (Sabina). O en la libre de hacer la propia responsabilidad. "La vida es riesgo e inseguridad, compromiso, temor y temblor" (Kierkegaard).

![](tim-mossholder-clN4DePMfm4-unsplash.jpg){fig-align="left"}

La posibilidad de las relaciones sociales armónicas, hace mucho que fueron descubiertas por la vida a través de los insectos, básicamente en el ejemplo del hormiguero o la colmena. Pero este modelo social no es válido a nivel humano, porque entre sus miembros no existe una asociación libre. Una hormiga no está capacitada para decidir si mañana irá a trabajar, o si hoy rendirá menos, o cuándo llegarán las vacaciones. Cada hormiga está integrada en su hormiguero. Forma parte de un todo social. Sin embargo, el yo, propenso al egoísmo, es libre para asociarse o no, para actuar de una forma u otra. Este paso del yo, al nosotros concienciado, suele fracasar a menudo, pues este yo es originalmente egoísta. El yo mira ante todo por sí mismo. Cuando unos valores económicos se consiguen gracias a unos conocimientos que dan lugar a grandes medios técnicos, estos pueden ser utilizados para mejorar el bien común (nosotros) o para enriquecer el bien particular (yo). Esto último es lo que ha primado desde siempre, pero adquiere mayor fuerza a partir de los últimos 124 años, a través de la era industrial y sus mejoras técnicas, tras las dos guerras mundiales. Actualmente, como nunca anteriormente había ocurrido, la inteligencia artificial triunfa a nivel global, a través de una extensa, manipuladora y sofisticada red digital, dirigida y orquestada por "Ellos"... La técnica actual ha dado lugar a una teocracia, en donde los replicantes, dirigen, estructuran, modelan y controlan "tras" las pantallas: "No te preocupes, ya pienso yo por ti". Actualmente todo el interés humano se limita al apego económico, como medio para conseguir el PODER. Se ha perdido el sentido de lo trascendente, en un mundo en donde sólo se vive el instante fatuo.

Cualquier entidad auto-referente, capaz de individualidad, cuando se pregunta: ¿Quién soy, de dónde vengo, y adónde voy? Está filosofando, pues los conceptos filosóficos surgen en base a cuestiones básicas que puedan aclarar y mejorar el existir humano: ¿Qué debo hacer? La filosofía no pretende ser una ciencia, sino un adelantado del conocimiento que traiga preguntas a la comprensión para poder transformarlas en saber. Una teorética del conocimiento, que aporte datos a una teorética del conocido, para llevarlos a medida y poder convertirlos en Ciencia.

La filosofía como **saber** acerca de las cosas.

La filosofía como **dirección** para el mundo.

La filosofía como **forma** de vida.

Vivimos una época en donde unos metalizados, gélidos y distantes teócratas ("Ellos") manipulan al resto del mundo a través de una extensa red digital, consiguiendo en la mayoría de la sociedad una ansiedad cada vez más egoísta sobre el instante de su frívolo **acontecer**, con el olvido de **Ser**. Esta amenaza era debida hasta hace poco, al criterio oportunista de unos pocos dirigentes religiosos o políticos. Actualmente ese dirigente no se llama Pío XII (1876-1958), Adolf Hitler (1889-1945), o Josip Stalin (1879-1953), sino que el Mundo del Siglo XXI, la época de la "I.A." está manejada por el anonimato férreo, soterrado, sibilino, evanescente e invisible del ente tecnológico ("Ello"). La información y el control técnico nos invaden sin notarlo, a través de una brisa íntima y familiar: ¡El **móvil**! Un pequeño aparato, demonio con cara de ángel, indispensable en todo hogar. Nos acompaña, nos "informa", nos condiciona y nos modela a gusto del "Ello". 16000000 de **móviles**, tejiendo una economía de 1600000000000 de dólares. A estos utensilios tan inocentes y "necesarios", les consultamos todo en nuestro quehacer diario. (Antes, cuando los niños lloraban, se les consolaba con el chupete, el sonajero, o una canción, ahora se les consuela con un **móvil**). No podemos subsistir sin estos artilugios. ¡Somos sus vasallos! Nos subyugan. "Podría vivir sin cualquier otra cosa, pero no sin mi móvil". "Por favor, quíteme todo, menos mi móvil". No se comprende una persona sin su **móvil**. ¿Usted no tiene móvil? ... Es lo que hay. Es lo que priva hoy día. Es lo que está de moda. ¡Yo soy mi **móvil**! Creo en lo que dice mi **móvil**. Me informo y aprendo con lo que me comunica mi **móvil**. Consulto a mi **móvil**. Hago lo que dice mi **móvil**. ¡No puedo vivir sin mi **móvil**!... ¡No me doy cuenta de que soy esclavo de mi **móvil** y por ello obedezco ciegamente su mandato. ¡Me fío de mi **móvil**, así vivo criopreservado!... ¡Bendito **móvil**!

En consecuencia, la sociedad del siglo XXI no se aferra a nada, no tiene certezas absolutas, nada le sorprende sobremanera, todo es superficial y cualquier opinión es susceptible de modificaciones rápidas. La "verdad" es lo que el medio puesto de moda dice que es la verdad. El "Ello" y los creadores de contenidos cada vez más diversos, chamanes, influencers de todo tipo, minorías étnicas, sexuales, religiosas, esotéricas, culturales o estéticas han tomado la palabra (cada día tiene menos valor) y el individuo postmoderno, subyugado por el **móvil**, sometido a una avalancha de informaciones y estímulos (casi siempre vulgares) difíciles de estructurar, ha optado por un vagabundeo incierto y superficial de unas ideas a otras. Todo esto se traduce en un desconfiado deshacerse de cualquier participación en la construcción de un mundo más solidario, y cada uno se limita a sobrevivir en el instante frívolo. ("Pienso, luego Yoigo").

<sup><sub> Fuente imagen: freepik.es. https://www.freepik.com/free-photo/back-view-woman-doing-yoga-sunset_11283026.htm#fromView=search&page=1&position=52&uuid=cd4a3200-cd1e-439e-8880-0ffc987541bd </sub></sup>
