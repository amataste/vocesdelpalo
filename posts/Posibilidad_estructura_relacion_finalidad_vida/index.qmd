---
title: "Posibilidad, estructura, relación y finalidad: vida"
author: "Manuel Carabias"
date: "2024-06-14"
categories: [Filosofía, Existenciariedad, Estructura, Ser-siendo, Ser]
image: "ed9d5452-f385-4a9e-896c-18c476ab9c04.webp"
draft: false
---

Se llama estructura a la distribución, el orden y la intención en que están dispuestos los elementos que agrupan una entidad; conjunto de relaciones entre el compuesto de un ente. Punto. Concibo la existenciariedad como un conglomerado de niveles de existencia, constituido por series ilimitadas de estructuras formadas cada una por el modo de estar de elementos solidarios entre sí (extensos y duraderos) de forma que no puede modificarse uno de ellos, sin que esto afecte al resto de la estructura. Aquello que básicamente define una estructura es la relación entre sus elementos: conexión o enlace (correspondencia) entre términos de una misma entidad. Gracias a la estructura puede darse la vida, como esa forma especial de organización de la materia que se presenta en la naturaleza y se caracteriza por determinados procesos físicos y químicos que dan lugar a seres complejos estructurales que pueden organizarse, alimentarse, relacionarse, reproducirse y evolucionar, hasta el punto de ser capaces de decir: YO.

Así pues, la estructura conforma un conjunto o grupo de elementos funcionalmente correlacionados. Un conglomerado dinámico como sistema de transformaciones que se autorregulan con un fin. Los elementos en cuestión de la estructura son considerados como miembros más bien que como partes. El conjunto o grupo es un todo y no hay una “mera suma”. Por eso en la descripción de una estructura salen a relucir vocablos como articulación, compenetración funcional y, a veces, solidaridad e intención.

¿Las estructuras actúan con vistas a un fin determinado? Yo diría que las estructuras funcionan en base a la posibilidad, y su realización consiste en garantizar esa posibilidad. Según esta idea, todos los posibles se realizan. Posible es en todo aquello que puede ser tractado o sacado a la existencia por la necesidad. Solo no es posible lo contradictorio. Toda la existenciariedad, como un conjunto de niveles de existencia es posibilidad. Y esa posibilidad da lugar a una determinación: tal, que significa algo o alguien, existiendo, consistiendo, y aconteciendo.

**Existir**

Existir consiste en estar-ahí, y no solo en estar-ahí, sino estar hallándose, llegando al conocimiento de cualquier realidad sobre la experiencia inmediata de la existencia propia. Existencia es la realidad concreta de un ente y por antonomasia, la existencia auto-referente. La existenciariedad, como conjunto de niveles posibles de existencia, es tributaria de la paradoja de Ser en el estar y del estar en Ser. Todo lo que está, necesita ser pero no todo él Es, está. Es decir, el Ser no se agota en el estar. El Es antecede, sobrevive y prevalece siempre al estar. Por ello, las posibilidades de ser y por tanto de estar, son infinitas. Estar es acontecer, hallarse en cualquier momento y circunstancia en una situación estructural determinada, siendo duradera y extensa. Aquello que no dura, no puede estar ¿Cuándo? Aquello que no es extenso, no puede estar ¿Dónde? No es posible una entidad estructural sin relaciones duraderas y extensas. Sin embargo, el infinito instante de Ser, hace posible los ilimitados y enriquecedores momentos del estar siendo. Lo Real es Ser, y existen infinitas apariencias de ser-siendo. Por ello, Ser no se limita a su Real seidad (mismidad) sino que en su figurar, da lugar a infinitas entidades y relaciones de seres siendo ¿Por qué?

Ser es Ser. No puede predicarse, pero se expresa a través de todo posible ser-siendo. Soy Yo, se manifiesta a “través” de ser-siendo: yo-soy. Soy Yo, se sabe a través de yo-soy, y yo-soy es posible gracias a soy-Yo. Yo-soy ser-siendo (razón) y ser-siendo soy-Yo (referente). Lo Real (Ser) hace posible lo relativo y lo relativo (ser-ahí) da razón de lo real. Sólo hay Ser, e infinitas apariencias, relaciones o manifestaciones de Ser-siendo. Se hace entonces necesario distinguir entre Ser (Esse) y el hecho de que algo sea (ens). Entre Ser y estar (aparecer). Las relaciones ocultan al Ser.

El mencionado concepto de ser es un juicio límite que carece de toda significación. No puede tener accidentes ni atributos; no puede tampoco encerrarse en ninguna categoría, es pues, completamente opaco e impenetrable. No designa ninguna realidad y sí únicamente una tendencia que puede y debe poseer cualquier realidad. Se trata entonces de una enajenación de Ser, pero de una enajenación necesaria. Solo podemos elucubrar y trabajar con seres relativos, pero siempre con trascendencia, es decir, sin perder nunca el referente: ¡Ser! Lo que está ahí, a la mano, lo que existe, lo relativo, no puede ignorar en ningún momento lo Real. Por ello, la Ciencia, no debería olvidar en ningún momento esta evanescencia referencial: nunca se podrá llegar a un conocimiento exhaustivo del mundo de las relaciones sin un referente continuo hacia lo Real. El mundo del conocimiento, nos guste o no, es una búsqueda siempre inacabada del yo-soy hacia el soy-Yo. Toda la serie numérica tiende hacia infinito, pero nunca lo alcanza. El mundo relativo de las relaciones, indaga lo Real y siempre naufraga en la paradoja. Ser-ahí busca a Ser y en todo momento zozobra, pues Ser está en todos sitios, pero no se encuentra en parte alguna. No conseguimos salir del círculo vicioso: nada busca a Ser, para ser Nada. ¡Hay, qué tragedia tan alta!

Lo intenté, abriendo resquicios, pues a través de ellos, algo se debe vislumbrar… ¿Qué? ¿Cuándo? ¿Cómo? ¿Dónde? “**ES**”.

Por estar en todos sitios al mismo tiempo, no se le encuentra en ninguna parte, ni momento.

0=&=0=&=0=&=0=&=0=&=0=&=0

Esta es la expresión matemática de lo Real (Tautológico): ¡ES! Tiempo infinito, sin resquicio de Espacio. Real. Ser.

0\>1\>2\>3\>4\>5\>6\>7\>8\>9\> ……………&

Esta es la expresión matemática de lo relativo (Existenciario): Fue\> (siendo) \> será. Relativo espacio-tiempo. Niveles de realidad. Casi-Nada \> ser-ahí \> Casi-Ser.

Todo aquello que implica relación, aparece al ser-ahí como algo (tal) digno de análisis. Absoluto Ser, aparece al ser-ahí, como Nada. Lo Real, está “oculto” por las relaciones. Lo Real, Es, sin relaciones. ¿A qué las relaciones? Una de dos, o admitiendo que toda la existenciariedad es apariencia o ilusión. O admitiendo que cada momento (instante) del cambio, el ahí de cada entidad constitutiva de la existenciariedad, es **infinito**. Cada momento del cambio, no solo es único e irrepetible, sino, además infinito. Cada momento del cambio, cada instante como actuación existenciaria, repercute en el todo de la existenciariedad; de ahí la grandiosidad y la gran responsabilidad del acto.
